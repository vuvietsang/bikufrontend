import firebase from "firebase/app";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyBaq0azcv7kufPMja57JgB197pnt3SAuck",
  authDomain: "storeimg-c0ebd.firebaseapp.com",
  projectId: "storeimg-c0ebd",
  storageBucket: "storeimg-c0ebd.appspot.com",
  messagingSenderId: "301447696951",
  appId: "1:301447696951:web:1d3c78375a48c4c57965b2",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { storage, firebase as default };
