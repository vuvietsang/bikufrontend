import React from 'react';
import classNames from 'classnames';
import { useLocation } from 'react-router';
import { NavLink } from 'react-router-dom'

MainMenu.propTypes = {

};

function MainMenu(props) {

  const location = useLocation();
  const tab = location.pathname;
  const user = JSON.parse(localStorage.getItem('user'));
  return (
    <nav className="navbar-default navbar-side" role="navigation">
      <div className="sidebar-collapse">
        <ul className="nav" id="main-menu" style={{ display: 'block' }}>
          <li className="text-center">
            <img src="/assets/img/find_user.png" className="user-image img-responsive" />

            <h2 style={{ color: 'white' }}>{user && user.name}</h2>
          </li>
          {user.role.roleName === 'MODIFIER' &&
            <li>
              <NavLink className={classNames({
                "active-menu": tab.includes('/admin/bikes')
              })} to="/admin/bikes"><i className="fa fa-dashboard fa-3x" /> Products</NavLink>
            </li>
          }
          {user.role.roleName === 'ADMIN' &&
            <li>
              <NavLink className={classNames({
                "active-menu": tab.includes('/admin/bikes')
              })} to="/admin/bikes"><i className="fa fa-dashboard fa-3x" /> Products</NavLink>
            </li>
          }
          {user.role.roleName === 'ADMIN' &&
            <li>
              <NavLink className={classNames({
                "active-menu": tab.includes('/admin/accounts')
              })} to="/admin/accounts"><i className="fa fa-dashboard fa-3x" /> Accounts</NavLink>
            </li>
          }
          {user.role.roleName === 'SALER' &&
            <li>
              <NavLink className={classNames({
                "active-menu": tab.includes('/admin/orders')
              })} to="/admin/orders"><i className="fa fa-dashboard fa-3x" /> Orders</NavLink>
            </li>
          }
          {user.role.roleName === "ADMIN" &&
            <li>
              <NavLink className={classNames({
                "active-menu": tab.includes('/admin/orders')
              })} to="/admin/orders"><i className="fa fa-dashboard fa-3x" />Orders</NavLink>
            </li>
          }
        </ul>
      </div>
    </nav>
  );
}

export default MainMenu;