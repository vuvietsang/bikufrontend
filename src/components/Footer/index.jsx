import React from 'react';
import MessengerCustomerChat from 'react-messenger-customer-chat';

Footer.propTypes = {

};

function Footer(props) {
  return (
    <div>
      <div className="instagram-box">
        <div className="main-instagram owl-carousel owl-theme">
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-01.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-02.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-03.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-04.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-05.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-06.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-07.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-08.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-09.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
          <div className="item">
            <div className="ins-inner-box">
              <img src="/images/instagram-img-05.jpg" alt="" />
              <div className="hov-in">
                <a href="https://www.instagram.com/stories/highlights/17893488298979321/"><i className="fab fa-instagram" /></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* End Instagram Feed  */}
      {/* Start Footer  */}
      <footer>
        <div className="footer-main">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-12 col-sm-12">
                <div className="footer-widget">
                  <h4>Về Gentlement stylez</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </p>
                  <ul>
                    <li><a href="https://www.facebook.com/hieuthomnghiep"><i className="fab fa-facebook" aria-hidden="true" />.</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep"><i className="fab fa-twitter" aria-hidden="true" />.</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep"><i className="fab fa-linkedin" aria-hidden="true" />.</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep"><i className="fab fa-google-plus" aria-hidden="true" />.</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep"><i className="fa fa-rss" aria-hidden="true" />.</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep"><i className="fab fa-pinterest-p" aria-hidden="true" />.</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep"><i className="fab fa-whatsapp" aria-hidden="true" />.</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4 col-md-12 col-sm-12">
                <div className="footer-link">
                  <h4>Thông tin</h4>
                  <ul>
                    <li><a href="/about">Về chúng tôi</a></li>
                    <li><a href="/service">CSKH</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep">Quyền hạn các nhân</a></li>
                    <li><a href="https://www.facebook.com/hieuthomnghiep">Thông tin giao hàng</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-4 col-md-12 col-sm-12">
                <div className="footer-link-contact">
                  <h4>Liên hệ với chúng tôi</h4>
                  <ul>
                    <li>
                      <p><i className="fas fa-map-marker-alt" />Địa chỉ: Michael I. Days 3756 <br />Preston Street Wichita,<br /> KS 67213 </p>
                    </li>
                    <li>
                      <p><i className="fas fa-phone-square" />SĐT: <a href="tel:+1-888705770">+1-888 705 770</a></p>
                    </li>
                    <li>
                      <p><i className="fas fa-envelope" />Email: <a href="mailto:contactinfo@gmail.com">contactinfo@gmail.com</a></p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      {/* End Footer  */}
      {/* Start copyright  */}
      <div className="footer-copyright">
        <p className="footer-company">All Rights Reserved. © 2018 <a href="https://www.facebook.com/hieuthomnghiep">ThewayShop</a> Design By :
          <a href="https://html.design/">html design</a></p>
      </div>
      {/* End copyright  */}
      <MessengerCustomerChat pageId="100991585735608" appId="488883578985747" />
      <a href="" id="back-to-top" title="Back to top" >↑</a>
    </div>
  );
}

export default Footer;