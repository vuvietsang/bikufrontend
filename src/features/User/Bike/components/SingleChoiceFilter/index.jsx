import React from 'react';

SingleChoiceFilter.propTypes = {

};

function SingleChoiceFilter({ form }) {


  const { register } = form;
  return (
    <div className="filter-brand-left">
      <div className="title-left">
        <h3>Giá tiền</h3>
      </div>
      <div className="brand-box">
        <ul>
          <li>
            <div className="radio radio-danger">
              <input {...register("price")} id="Radios1" value="100000" type="radio" />
              <label htmlFor="Radios1"> Trên 1000 </label>
            </div>
          </li>
          <li>
            <div className="radio radio-danger">
              <input {...register("price")} id="Radios2" value="400-1000" type="radio" />
              <label htmlFor="Radios2"> 400-1000 </label>
            </div>
          </li>
          <li>
            <div className="radio radio-danger">
              <input {...register("price")} id="Radios3" value="200-400" type="radio" />
              <label htmlFor="Radios3"> 200-400 </label>
            </div>
          </li>
          <li>
            <div className="radio radio-danger">
              <input {...register("price")} id="Radios4" value="50-200" type="radio" />
              <label htmlFor="Radios4"> 50-200 </label>
            </div>
          </li>
          <li>
            <div className="radio radio-danger">
              <input {...register("price")} id="Radios5" value="0-50" type="radio" />
              <label htmlFor="Radios5"> 0 - 50 </label>
            </div>
          </li>
          {/* <li>
            <div className="radio radio-danger">
              <input {...register("price")} id="Radios5" value="default" type="radio" />
              <label htmlFor="Radios6">  Default </label>
            </div>
          </li> */}

        </ul>
      </div>
    </div>
  );
}

export default SingleChoiceFilter;