import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { findAllInRenderedTree } from 'react-dom/test-utils';
import { array } from 'yup';
import reducer from '../../../Checkout/counterSlice';
import { NavLink } from 'react-router-dom';

DefaultBikes.propTypes = {
  defaultBikes: PropTypes.array,
  addToCart: PropTypes.func.isRequired,
};

DefaultBikes.defaultProps = {
  defaultBikes: [],
  addToCart: null,
}

function DefaultBikes({ defaultBikes, addToCart, viewDetail }) {
  const HandleAddToCart = (bike) => {
    if (addToCart) {
      addToCart(bike);
    }
  }



  return (
    <div className="row product-categorie-box">
      <div className="tab-content">
        <div role="tabpanel" className="tab-pane fade show active" id="grid-view">
          <ul className="row">
            {defaultBikes.map((bike) => (
              <li className="col-sm-6 col-md-6 col-lg-4 col-xl-4" key={bike.id}>
                <div className="products-single fix">
                  <div className="box-img-hover">
                    <div className="type-lb">
                      <p className="sale">Sale</p>
                    </div>
                    <img src={bike.imgURL} className="img-fluid" alt="This a pic" />

                    <div className="mask-icon">
                      <ul>
                        <li> <NavLink to={`/bikes/detail?id=${bike.id}`} ><i className="fas fa-eye" /></NavLink></li>
                      </ul>
                      <button className="cart" onClick={() => { HandleAddToCart(bike) }}>Thêm vào giỏ hàng</button>
                    </div>
                  </div>
                  <div className="why-text">
                    <h4>{bike.name}</h4>
                    <h5> ${bike.price} </h5>
                  </div>
                </div>
              </li>
            ))}
          </ul>

        </div>
      </div>
    </div>


  );
}

export default DefaultBikes;






