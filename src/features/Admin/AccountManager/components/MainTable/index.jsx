/* eslint-disable react-hooks/exhaustive-deps */
import { Button, Dialog, DialogContent } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import accountApi from '../../../../../api/accountApi';
import CreateAccount from '../../pages/CreateAccount';
import UpdateAccount from '../../pages/UpdateAccount';
import PaginationCompnent from '../Pagination';
MainTable.propTypes = {

};

function MainTable(props) {

    const [accounts, setAccounts] = useState([]);
    const [open, setOpen] = useState(false);

    const [filter, setFilter] = useState({
        pageNum: 0,
        pageSize: 10,
        totalPage: 1,
    })

    useEffect(async () => {
        const response = await accountApi.getAllAccounts(filter.pageNum, filter.pageSize);
        setFilter({ ...filter, totalPage: response.data.data.totalPages });
    }, [])

    const initialAccounts = async () => {
        const response = await accountApi.getAllAccounts(filter.pageNum, filter.pageSize);
        setAccounts(response.data.data.content);
    }

    useEffect(() => {
        initialAccounts();
    }, [filter])

    const handleClickOpen = () => {
        setOpen(true);
        setMode('create');
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleUpdated = () => {
        setOpen(false);
        setFilter({
            ...filter, PageNumber: filter, totalPage: filter ? filter.totalPage + 1 : 3
        })

    }
    const [updateAccount, setUpdateAccount] = useState(null);
    const [mode, setMode] = useState('create');
    const handleUpdate = (account) => {
        setOpen(true);
        setUpdateAccount(account);
        setMode('update');
    }

    const handleDeleteClick = (id) => {
        accountApi.deleteUserById(id);
        setFilter({ ...filter, hi: "hi" })
    }
    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogContent>
                    {mode === 'create' &&
                        <CreateAccount handleUpdated={handleUpdated} />
                    }
                    {mode === 'update' &&
                        <UpdateAccount account={updateAccount} handleUpdated={handleUpdated} />
                    }
                </DialogContent>
            </Dialog>

            {/* /. NAV SIDE  */}
            <div id="page-wrapper">
                <div id="page-inner">
                    <div className="row">
                        <div className="col-md-12">
                            <h2>Account Manager</h2>
                            <h5>Welcome to account manager page. </h5>
                        </div>
                    </div>
                    {/* /. ROW  */}
                    <hr />
                    <div className='register'>


                        <Button variant="outlined" color="primary" onClick={handleClickOpen} className="btn icon-btn btn-success" >  <span className="glyphicon btn-glyphicon glyphicon-plus img-circle text-success " />
                            Create account </Button></div>
                    <div className="row">
                        <div className="col-md-12">

                            {/* Advanced Tables */}
                            <div className="panel panel-default">

                                <div className="panel-heading">
                                    Account Table
                                </div>
                                <div className="panel-body">
                                    <div className="table-responsive">
                                        <table className="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Email</th>
                                                    <th>UserName</th>
                                                    <th>Full Name</th>
                                                    <th>Role</th>
                                                    <th>Delete</th>
                                                    <th>Update</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {accounts && accounts.map(account => (
                                                    <tr className="odd gradeX" key={account.email}>
                                                        <td>{account.email}</td>
                                                        <td>{account.userName}</td>
                                                        <td>{account.name}</td>
                                                        <td>{account.role.roleName}</td>
                                                        <td><button onClick={() => handleDeleteClick(account.id)} className="btn btn-danger"><i className="fa fa-pencil"></i> Delete</button></td>
                                                        <td><button onClick={() => handleUpdate(account)} className="btn btn-primary"><i className="fa fa-pencil"></i> Update</button></td>
                                                    </tr>
                                                ))
                                                }
                                            </tbody>
                                        </table>
                                        <PaginationCompnent filter={filter} setFilter={setFilter} />
                                    </div>
                                </div>
                            </div>
                            {/*End Advanced Tables */}
                        </div>
                    </div>
                </div>
                {/* /. PAGE INNER  */}
            </div>
        </div>
    );
}

export default MainTable;
