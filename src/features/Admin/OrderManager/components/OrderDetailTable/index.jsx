import React, { useState } from 'react';
import { useSnackbar } from 'notistack';
import queryString from 'query-string';
import { useHistory, useLocation } from 'react-router-dom';
import orderApi from '../../../../../api/orderApi';
import { useQuery, useMutation, useQueryClient } from 'react-query';
function OrderDetailTable() {
    const history = useHistory();
    const location = useLocation();
    const param = queryString.parse(location.search);
    const orderId = param.id;
    const queryClient = useQueryClient();
    let [preOrder, setPreOrder] = useState();

    useQuery("getOrderById", () => {
        return orderApi.get(orderId);
    }, {
        onSuccess: (value) => {
            setPreOrder(value.data.data);
            console.log(preOrder);
        }
    })
    const { mutate: confirm } = useMutation(() => {
        return orderApi.confirm(orderId)
    }, {
        onSettled: () => {
            queryClient.invalidateQueries("getorders");
            history.replace('/admin/orders');
            enqueueSnackbar('Successfully', { variant: 'success' })

        }
    })
    const { enqueueSnackbar } = useSnackbar();
    const handleConfirm = async () => {
        try {
            confirm();


        } catch (error) {
            enqueueSnackbar(error.message, { variant: 'error' });
        }
    }
    const handleCancel = () => {
        history.replace('/admin/orders')
    }
    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    {/* Advanced Tables */}

                    <div className="panel panel-default ">
                        <div className="panel-heading">
                            Order Details
                        </div>
                        <div className="panel-body">
                            <div className="table-responsive">
                                <table className="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Product Id</th>
                                            <th>Product Name</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                            <th>Create Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {preOrder?.orderedProduct.map((orderDetail) => (
                                            <tr className="gradeU" key={orderDetail.product.id}>
                                                <td>{orderDetail.product.id}</td>
                                                <td>{orderDetail.product.name}</td>
                                                <td>{orderDetail.quantity}</td>
                                                <td>{orderDetail.price}</td>
                                                <td>20-10-2021</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                                {preOrder?.status === false && <button onClick={handleConfirm} className="btn btn-glyphicon"><i className="fa fa-pencil"></i> Confirm</button>}
                                <button onClick={handleCancel} style={{ float: 'right' }} className="btn btn-danger"><i className="fa fa-pencil"></i> Cancel</button>
                            </div>
                        </div>
                    </div>
                    {/*End Advanced Tables */}
                </div>




            </div>
        </div>
    );
}
export default OrderDetailTable;