import { axiosClient } from "./axiosClient";

const accountApi = {
  register(data) {
    const url = `user/register`;
    return axiosClient.post(url, data);
  },
  login(data) {
    const url = "user/authenticate";
    return axiosClient.post(url, data);
  },
  async getAllAccounts(pageNumer, pagesize) {
    const url = `user/users?pageNum=${pageNumer}&pageSize=${pagesize}`;
    const data = await axiosClient.get(url);
    return data;
  },
  async registerByAdmin(data) {
    const url = "/user/admin/register";
    return await axiosClient.post(url, data);
  },
  async updateByAdmin(id, data) {
    const url = `/user/update/${id}`;
    return await axiosClient.put(url, data);
  },
  async deleteUserById(userid) {
    const url = `user/deleteUser/${userid}`;
    return await axiosClient.delete(url);
  },
};
export default accountApi;
