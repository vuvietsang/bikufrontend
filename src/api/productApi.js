import { axiosClient } from "./axiosClient";

const productApi = {
  async getAll(filter) {
    var url = "product/products";
    if (filter.searchBy) {
      if (filter.searchBy === "Maker") {
        url = `product/products/maker/${filter.value}`;
      } else if (filter.searchBy === "Name") {
        url = `product/products/name/${filter.value}`;
      }
    } else if (filter.priceRange) {
      if (filter.priceRange !== "default") {
        var [min, max] = filter.priceRange.split("-");
        if (filter.priceRange === "100000") {
          min = 8000;
          max = 100000;
        }
        url = `product/products/price/${min}/${max}`;
      }
    }
    const response = await axiosClient.get(url, {
      params: filter,
    });
    return {
      data: response.data,
      pagination: {
        total: response.data.data.totalPages,
        page: filter.pageNum,
        PageSize: filter.pageSize,
      },
    };
  },

  async get(id) {
    const url = `product/products/${id}`;
    return await axiosClient.get(url);
  },

  async update(id, data) {
    console.log(data);
    const url = `product/products/${id}`;
    return await axiosClient.put(url, data);
  },

  async add(data) {
    const url = "product/products/add";
    return await axiosClient.post(url, data);
  },

  delete(id) {
    const url = `product/products/${id}`;
    return axiosClient.delete(url);
  },
};

export default productApi;
